public class Workout {
	
	private String title;
	private float cal;
	
	public Workout() {}
	
	public Workout(String title, float cal) {
		this.title = title;
		this.cal = cal;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public float getCal() {
		return cal;
	}

	public void setCal(float cal) {
		this.cal = cal;
	}

}
