import java.util.Arrays;
import java.util.List;

public class Week {
	
	private Workout exercise;
	private List<Float> hours;
	
	public Week(Workout exercise) {
		this.exercise = exercise;
		this.hours = Arrays.asList(0f, 0f, 0f, 0f, 0f, 0f, 0f);
	}
	
	public Workout getExercise() {
		return exercise;
	}
	
	public void setExercicie(Workout exercise) {
		this.exercise = exercise;
	}

	public List<Float> getHours() {
		return hours;
	}

	public void setHours(List<Float> hours) {
		this.hours = hours;
	}

}
