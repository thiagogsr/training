import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("trainingMB")
@SessionScoped
public class Training implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<String> days;
	private ArrayList<Week> weeks;
	private String student;
	private byte weight;
	private Workout objWorkout;
	private List<Float> dayCount;
	private float weekCount;
	
	public Training() {
		this.objWorkout = new Workout();
		this.weeks = new ArrayList<Week>();
		this.dayCount = Arrays.asList(0f, 0f, 0f, 0f, 0f, 0f, 0f);
		this.days = Arrays.asList("Domingo", "Segunda", "Ter�a", "Quarta", "Quinta", "Sexta", "S�bado"); 
	}
	
	public void addWeek() {
		if(this.objWorkout.getTitle() != "" && this.objWorkout.getCal() != 0) {
			this.weeks.add(new Week(new Workout(this.objWorkout.getTitle(), this.objWorkout.getCal())));
			this.objWorkout = new Workout();
		}
	}
	
	public void removeWeek(Week idx) {
		this.weeks.remove(idx);
		this.calc();
	}
	
	public void calc() {
		this.dayCount = Arrays.asList(0f, 0f, 0f, 0f, 0f, 0f, 0f);
		for (Week w : this.weeks) {
			for (int c = 0; c < w.getHours().size(); c++) {
				Float count = w.getHours().get(c) * w.getExercise().getCal();
				this.dayCount.set(c, this.dayCount.get(c) + count);
			}
		}
		
		this.weekCount = 0f;
		for (int i = 0; i < this.dayCount.size(); i++) {
			this.weekCount = this.weekCount + this.dayCount.get(i);
		}	
	}
	
	public List<String> getDays() {
		return days;
	}
	
	public void setDays(List<String> days) {
		this.days = days;
	}
	
	public byte getWeight() {
		return weight;
	}
	
	public void setWeight(byte weight) {
		this.weight = weight;
	}

	public String getStudent() {
		return student;
	}

	public void setStudent(String student) {
		this.student = student;
	}

	public Workout getObjWorkout() {
		return objWorkout;
	}

	public void setObjWorkout(Workout objWorkout) {
		this.objWorkout = objWorkout;
	}

	public ArrayList<Week> getWeeks() {
		return weeks;
	}

	public void setWeeks(ArrayList<Week> weeks) {
		this.weeks = weeks;
	}

	public List<Float> getDayCount() {
		return dayCount;
	}

	public void setDayCount(List<Float> dayCount) {
		this.dayCount = dayCount;
	}

	public float getWeekCount() {
		return weekCount;
	}

	public void setWeekCount(float weekCount) {
		this.weekCount = weekCount;
	}

}
